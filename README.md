# ciecolors #

ciecolors provides conversion functions between sRGB, XYZ, L*a*b*, and L*u*v*, as well as a CIEDE2000 L*a*b* color difference function.


## Example usage ##

    >>> from ciecolors import *
    >>> rgb2lab(60, 50, 40)
    (21.561303463754875, 2.6356997311619, 8.035588613081146)
    >>> labdiff((50, 2.5, 0), (56, -27, -3))
    31.90300464686388


## Requirements ##

ciecolors has been tested to work on Python 2.7 and 3.3 with no additional dependencies.
