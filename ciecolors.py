#!/usr/bin/env python

# Copyright (c) 2016 Johannes Sasongko <sasongko@gmail.com>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


"""Various functions to work with sRGB, XYZ, L*a*b*, and L*u*v* color spaces.

This module provides conversion functions between:
* sRGB <-> XYZ (based on [WP_sRGB]);
* XYZ <-> L*a*b* (CIELAB) (based on [WP_Lab]);
* XYZ <-> L*u*v* (CIELUV) (based on [WP_CIELUV]);
as well as the indirect conversions between them.

In addition, it also provides an L*a*b* color difference function adhering to
CIEDE2000 (based on [Sharma2000]).

RGB values are [0,255], while Y and L* values are [0.0,100.0]. The other values
are also scaled by 100, so for most (but not all) colors, XZ values are around
[0.0,100.0] while a*b* and u*v* values are around [-100.0,100.0].

Xn, Yn, Zn are from Illuminant D65 with standard (2 degrees) observer [WP_D65].
u'n and v'n are calculated from these.

For simplicity, the function names and parameters use the labels rgb, xyz, lab,
and luv. They invariably refer to the above color spaces.

[Sharma2000] http://www.ece.rochester.edu/~gsharma/ciede2000/ciede2000noteCRNA.pdf
[WP_CIELUV] https://en.wikipedia.org/w/?title=CIELUV&oldid=682703164
[WP_D65] https://en.wikipedia.org/w/?title=Illuminant_D65&oldid=691366293
[WP_Lab] https://en.wikipedia.org/w/?title=Lab_color_space&oldid=716165948
[WP_sRGB] https://en.wikipedia.org/w/?title=SRGB&oldid=713902212
"""


from __future__ import division, print_function

from itertools import permutations
import sys

if sys.version_info[0] >= 3:
    _round = round
else:
    def _round(*a, **kw):
        return int(round(*a, **kw))

def _args(a, a1, a2):
    """Helper to accept either one or three arguments"""
    if a1 is a2 is None:
        return a
    else:
        return a, a1, a2

__all__ = ['%s2%s' % x for x in permutations(('rgb', 'lab', 'luv', 'xyz'), 2)]
__all__ += ['labdiff']


# sRGB <-> XYZ

def rgb2xyz(r_or_rgb, g=None, b=None):
    """Convert sRGB to XYZ"""

    _0_04045_mul_255 = 10.31475
    _12_92_mul_255 = 3294.6
    a_mul_255 = 14.025
    _1_add_a__mul_255 = 269.025

    R, G, B = _args(r_or_rgb, g, b)

    def linearize(Csrgb):
        if Csrgb <= _0_04045_mul_255:
            return Csrgb / _12_92_mul_255
        else:
            return ((Csrgb + a_mul_255) / _1_add_a__mul_255) ** 2.4
    Rlin, Glin, Blin = map(linearize, (R, G, B))

    # Our XYZ approximate [0,100] instead of [0,1], so the conversion matrix
    # has been multiplied by 100.
    X = 41.24 * Rlin + 35.76 * Glin + 18.05 * Blin
    Y = 21.26 * Rlin + 71.52 * Glin + 7.22 * Blin
    Z = 1.93 * Rlin + 11.92 * Glin + 95.05 * Blin
    return X, Y, Z

def xyz2rgb(x_or_xyz, y=None, z=None, clamp=False):
    """Convert XYZ to sRGB"""

    _12_92_mul_255 = 3294.6
    _1_add_a__mul_255 = 269.025
    _1_div_2_4 = 0.4166666666666667
    a_mul_255 = 14.025

    X, Y, Z = _args(x_or_xyz, y, z)

    # Our XYZ approximate [0,100] instead of [0,1], so the conversion matrix
    # has been divided by 100.
    Rlin = 0.032406 * X + -0.015372 * Y + -0.004986 * Z
    Glin = -0.009689 * X + 0.018758 * Y + 0.000415 * Z
    Blin = 0.000557 * X + -0.002040 * Y + 0.010570 * Z

    def gamma_correct(Clin):
        if Clin <= 0.0031308:
            Csrgb = _12_92_mul_255 * Clin
        else:
            Csrgb = _1_add_a__mul_255 * Clin ** _1_div_2_4 - a_mul_255
        Csrgb = _round(Csrgb)
        if clamp:
            return min(max(0, Csrgb), 255)
        else:
            return Csrgb
    return tuple(map(gamma_correct, (Rlin, Glin, Blin)))


# XYZ <-> L*a*b*

def xyz2lab(x_or_xyz, y=None, z=None):
    """Convert XYZ to L*a*b*"""

    _6div29_pow3 = 0.008856451679035631
    _1div3 = 0.3333333333333333
    _1div3_mul_29div6_pow2 = 7.787037037037037
    _4div29 = 0.13793103448275862

    Xn = 95.047
    Yn = 100.00
    Zn = 108.883

    X, Y, Z = _args(x_or_xyz, y, z)
    if Y <= 0:
        return 0., 0., 0.

    def f(t):
        if t > _6div29_pow3:
            return t ** _1div3
        else:
            return _1div3_mul_29div6_pow2 * t + _4div29
    f__Y_div_Yn = f(Y / Yn)
    Lstar = 116 * f__Y_div_Yn - 16
    astar = 500 * (f(X / Xn) - f__Y_div_Yn)
    bstar = 200 * (f__Y_div_Yn - f(Z / Zn))
    return Lstar, astar, bstar

def lab2xyz(l_or_lab, a=None, b=None):
    """Convert L*a*b* to XYZ"""

    delta = 0.20689655172413793
    _3_delta_pow2 = 0.12841854934601665
    _3_delta_pow2_mul_4div29 = 0.017712903358071262

    Xn = 95.047
    Yn = 100.00
    Zn = 108.883

    Lstar, astar, bstar = _args(l_or_lab, a, b)
    if Lstar <= 0:
        return 0., 0., 0.

    def invf(t):
        if t > delta:
            return t ** 3
        else:
            return _3_delta_pow2 * t - _3_delta_pow2_mul_4div29
    _1div116_mul__Lstar_add_16 = (Lstar + 16) / 116
    X = Xn * invf(_1div116_mul__Lstar_add_16 + astar/500)
    Y = Yn * invf(_1div116_mul__Lstar_add_16)
    Z = Zn * invf(_1div116_mul__Lstar_add_16 - bstar/200)
    return X, Y, Z


# XYZ <-> L*u*v*

def xyz2luv(x_or_xyz, y=None, z=None):
    """Convert XYZ to L*U*V*"""

    _6div29_pow3 = 0.008856451679035631
    _29div3_pow3 = 903.2962962962963
    _1div3 = 0.3333333333333333

    Yn = 100.00
    unprime = 0.19783982482140774
    vnprime = 0.4683363029324097

    X, Y, Z = _args(x_or_xyz, y, z)
    if Y <= 0:
        return 0., 0., 0.

    Y_div_Yn = Y / Yn
    if Y_div_Yn <= _6div29_pow3:
        Lstar = _29div3_pow3 * Y_div_Yn
    else:
        Lstar = 116 * Y_div_Yn ** _1div3 - 16

    _13_Lstar = 13 * Lstar
    X_add_15Y_add_3Z = X + 15*Y + 3*Z
    uprime = 4*X / X_add_15Y_add_3Z
    vprime = 9*Y / X_add_15Y_add_3Z

    ustar = _13_Lstar * (uprime - unprime)
    vstar = _13_Lstar * (vprime - vnprime)
    return Lstar, ustar, vstar

def luv2xyz(l_or_luv, u=None, v=None):
    """Convert L*u*v* to XYZ"""

    _3div29_pow3 = 0.0011070564598794539

    Yn = 100.00
    unprime = 0.19783982482140774
    vnprime = 0.4683363029324097

    Lstar, ustar, vstar = _args(l_or_luv, u, v)
    if Lstar <= 0:
        return 0., 0., 0.

    _13_Lstar = 13 * Lstar
    uprime = ustar / _13_Lstar + unprime
    vprime = vstar / _13_Lstar + vnprime

    if Lstar <= 8:
        Y = Yn * Lstar * _3div29_pow3
    else:
        Y = Yn * ((Lstar + 16) / 116) ** 3
    X = Y * (9*uprime) / (4*vprime)
    Z = Y * (12 - 3*uprime - 20*vprime) / (4*vprime)
    return X, Y, Z


# sRGB <-> L*a*b*

def rgb2lab(r_or_rgb, g=None, b=None):
    """Convert sRGB to L*a*b*"""
    return xyz2lab(rgb2xyz(r_or_rgb, g, b))

def lab2rgb(l_or_lab, a=None, b=None, clamp=False):
    """Convert L*a*b* to sRGB"""
    return xyz2rgb(lab2xyz(l_or_lab, a, b), clamp=clamp)


# sRGB <-> L*u*v*

def rgb2luv(r_or_rgb, g=None, b=None):
    """Convert sRGB to L*u*v*"""
    return xyz2luv(rgb2xyz(r_or_rgb, g, b))

def luv2rgb(l_or_luv, u=None, v=None, clamp=False):
    """Convert L*u*v* to sRGB"""
    return xyz2rgb(luv2xyz(l_or_luv, u, v), clamp=clamp)


# L*a*b* <-> L*u*v*

def lab2luv(l_or_lab, a=None, b=None):
    """Convert L*a*b* to L*u*v*"""
    return xyz2luv(lab2xyz(l_or_lab, a, b))

def luv2lab(l_or_luv, u=None, v=None):
    """Convert L*u*v* to L*a*b*"""
    return xyz2lab(luv2xyz(l_or_luv, u, v))


# L*a*b* color difference

def labdiff(lab1, lab2):
    """Calculate difference between two L*a*b* colors"""
    import math
    atan2 = math.atan2
    cos = math.cos
    exp = math.exp
    sin = math.sin
    sqrt = math.sqrt

    # Numbers in comments refer to equation numbers in [Sharma2000].

    L1star, a1star, b1star = lab1
    L2star, a2star, b2star = lab2

    # 2
    C1star = sqrt(a1star**2 + b1star**2)
    C2star = sqrt(a2star**2 + b2star**2)

    # 3
    Cbarstar = (C1star + C2star) / 2

    # 4
    Cbarstar_pow7 = Cbarstar ** 7
    _25pow7 = 6103515625
    G = 0.5 * (1 - sqrt(Cbarstar_pow7 / (Cbarstar_pow7 + _25pow7)))

    # 5
    _1add_G = 1 + G
    a1prime = _1add_G * a1star
    a2prime = _1add_G * a2star

    # 6
    C1prime = sqrt(a1prime**2 + b1star**2)
    C2prime = sqrt(a2prime**2 + b2star**2)

    # 7
    _2pi = 6.283185307179586
    _180div_pi = 57.29577951308232
    def modified_hue(bstar, aprime):
        if bstar == aprime == 0:
            hprime = 0
        else:
            hprime = atan2(bstar, aprime)
            if hprime < 0:
                hprime += _2pi
            hprime *= _180div_pi
        return hprime
    h1prime = modified_hue(b1star, a1prime)
    h2prime = modified_hue(b2star, a2prime)

    # 8
    DeltaLprime = L2star - L1star

    # 9
    DeltaCprime = C2prime - C1prime

    # 10
    C1prime_mul_C2prime = C1prime * C2prime
    if C1prime_mul_C2prime == 0:
        Deltahprime = 0
    else:
        h2prime_sub_h1prime = h2prime - h1prime
        if h2prime_sub_h1prime > 180:
            Deltahprime = h2prime_sub_h1prime - 360
        elif h2prime_sub_h1prime < -180:
            Deltahprime = h2prime_sub_h1prime + 360
        else:
            Deltahprime = h2prime_sub_h1prime

    # 11
    DeltaHprime = 2 * sqrt(C1prime_mul_C2prime) \
        * sin(Deltahprime/_180div_pi / 2)

    # 12
    Lbarprime = (L1star + L2star) / 2

    # 13
    Cbarprime = (C1prime + C2prime) / 2

    # 14
    h1prime_add_h2prime = h1prime + h2prime
    if C1prime_mul_C2prime == 0:
        hbarprime = h1prime_add_h2prime
    else:
        if abs(h2prime_sub_h1prime) > 180:
            if h1prime_add_h2prime < 360:
                hbarprime = (h1prime_add_h2prime + 360) / 2
            else:
                hbarprime = (h1prime_add_h2prime - 360) / 2
        else:
            hbarprime = h1prime_add_h2prime / 2

    # 15
    hbarprime_in_rad = hbarprime / _180div_pi
    _30deg_in_rad = 0.5235987755982989
    _6deg_in_rad = 0.10471975511965978
    _63deg_in_rad = 1.0995574287564276
    T = 1 \
        - 0.17 * cos(hbarprime_in_rad - _30deg_in_rad) \
        + 0.24 * cos(2*hbarprime_in_rad) \
        + 0.32 * cos(3*hbarprime_in_rad + _6deg_in_rad) \
        - 0.20 * cos(4*hbarprime_in_rad - _63deg_in_rad)

    # 16
    Deltatheta = 30 * exp(-((hbarprime-275)/25)**2)

    # 17
    Cbarprime_pow7 = Cbarprime ** 7
    RC = 2 * sqrt(Cbarprime_pow7 / (Cbarprime_pow7+_25pow7))

    # 18
    Lbarprime_sub_50__pow2 = (Lbarprime - 50) ** 2
    SL = 1 + 0.015*Lbarprime_sub_50__pow2 / sqrt(20+Lbarprime_sub_50__pow2)

    # 19
    SC = 1 + 0.045 * Cbarprime

    # 20
    SH = 1 + 0.015 * Cbarprime * T

    # 21
    RT = -sin(2 * Deltatheta / _180div_pi) * RC

    # 22
    kL = kC = kH = 1
    DeltaCprime_div_kC_div_SC = DeltaCprime / (kC*SC)
    DeltaHprime_div_kH_div_SH = DeltaHprime / (kH*SH)
    return sqrt(
        (DeltaLprime / (kL*SL)) ** 2
        + DeltaCprime_div_kC_div_SC ** 2
        + DeltaHprime_div_kH_div_SH ** 2
        + RT * DeltaCprime_div_kC_div_SC * DeltaHprime_div_kH_div_SH)


# Tests

def test():
    def almost_equal(a, b, places):
        if isinstance(a, tuple) and isinstance(b, tuple):
            return all(almost_equal(c, d, places) for c, d in zip(a, b))
        return _round(a - b, places) == 0
    def assert_almost_equal(a, b, places=7):
        assert almost_equal(a, b, places), "Expected almost equal: %r, %r" % (a, b)

    # The expected values have been calculated with high precision and should
    # be accurate given the constants in the cited sources.

    assert_almost_equal(rgb2xyz(60, 50, 40), (3.38708434884301540534, 3.39506424553599134923, 2.48427702493378898765))
    assert_almost_equal(rgb2xyz(6, 5, 4), (0.15128998968008255934, 0.15602501062344442421, 0.13700600983427426698))  # < sRGB threshold
    assert_almost_equal(xyz2rgb(60, 50, 40), (252, 164, 161))
    assert_almost_equal(xyz2rgb(0.23, 0.22, 0.21), (10, 7, 6))  # < sRGB threshold

    assert_almost_equal(xyz2lab(60, 50, 40), (76.06926101415556953560, 32.06774787205968931830, 15.50043908246943723490))
    assert_almost_equal(xyz2lab(0.6789, 0.5555, 0.4321), (5.01781092592592592593, 6.18205858948340957242, 2.47085811213939801506))  # < L* threshold
    assert_almost_equal(lab2xyz(60, 50, 40), (40.93330056882200992251, 28.12333429004879248842, 10.26804771593751281315))
    assert_almost_equal(lab2xyz(6, 50, 40), (2.30984202255115010866, 0.66423387592767231129, -2.07328161056213866907))  # < L* threshold

    assert_almost_equal(xyz2luv(60, 50, 40), (76.06926101415556953560, 59.55622091349624598395, 15.36223613812203946869))
    assert_almost_equal(xyz2luv(0.6789, 0.5555, 0.4321), (5.01781092592592592593, 4.28008347651064365101, 1.08867885351309444458))  # < L* threshold
    assert_almost_equal(luv2xyz(60, 50, 40), (31.89852695648889405451, 28.12333429004879248842, 11.11966004060389158285))
    assert_almost_equal(luv2xyz(6, 50, 40), (1.27778395291603550562, 0.66423387592767231129, -1.71612576410433480139))  # < L* threshold

    assert_almost_equal(rgb2lab(60, 50, 40), (21.56130346375486399564, 2.63569973116188487066, 8.03558861308114400119))
    assert_almost_equal(rgb2lab(6, 5, 4), (1.40936814225747631927, 0.12260148280438356253, 0.47028034489071790084))  # < sRGB and L* thresholds
    assert_almost_equal(lab2rgb(60, 50, 40), (237, 103, 77))
    assert_almost_equal(lab2rgb(1, 0, -1), (2, 4, 6))  # < sRGB and L* thresholds

    assert_almost_equal(rgb2luv(60, 50, 40), (21.56130346375486399564, 6.02919923799537900921, 7.38965383545180883240))
    assert_almost_equal(rgb2luv(6, 5, 4), (1.40936814225747631927, 0.19500110285862236778, 0.28273536337379084805))  # < sRGB and L* thresholds
    assert_almost_equal(luv2rgb(60, 50, 40), (195, 130, 79))
    assert_almost_equal(luv2rgb(1, 0, -1), (4, 3, 8))  # < sRGB and L* thresholds

    assert_almost_equal(lab2luv(60, 50, 40), (60.00000000000000000000, 104.42712242088259160617, 34.67909225409629610872))
    assert_almost_equal(lab2luv(6, 50, 40), (6.00000000000000000000, 103.61864259015100366308, 40.49822634707202189609))  # < L* threshold
    assert_almost_equal(luv2lab(60, 50, 40), (60.00000000000000000000, 19.88120213926861884238, 37.54979692920329240493))
    assert_almost_equal(luv2lab(6, 50, 40), (6.00000000000000000000, 24.06177940935548535085, 34.89142326283950300844))  # < L* threshold

    # Random sanity tests. Since all conversions go through XYZ, we simply test
    # that converting from another color space to XYZ then back to the original
    # color space produces the original values.

    import random
    def rand_color():
        cs = random.choice(['rgb', 'lab', 'luv'])
        if cs == 'rgb':
            cols = tuple(random.randint(0, 255) for i in range(3))
        else:
            cols = (100*random.random(), 200*random.random() - 100, 200*random.random() - 100)
        return cs, cols
    for i in range(10000):
        g = globals()
        cs, cols = rand_color()
        cols1 = g['xyz2'+cs](g[cs+'2xyz'](cols))
        assert_almost_equal(cols, cols1)

    # CIEDE2000 test data from [Sharma2000]

    assert_almost_equal(labdiff((50, 2.6772, -79.7751), (50, 0, -82.7485)), 2.0425, 4)  # 1
    assert_almost_equal(labdiff((50, 3.1571, -77.2803), (50, 0, -82.7485)), 2.8615, 4)  # 2
    assert_almost_equal(labdiff((50, 2.8361, -74.0200), (50, 0, -82.7485)), 3.4412, 4)  # 3
    assert_almost_equal(labdiff((50, -1.3802, -84.2814), (50, 0, -82.7485)), 1.0000, 4)  # 4
    assert_almost_equal(labdiff((50, -1.1848, -84.8006), (50, 0, -82.7485)), 1.0000, 4)  # 5
    assert_almost_equal(labdiff((50, -0.9009, -85.5211), (50, 0, -82.7485)), 1.0000, 4)  # 6
    assert_almost_equal(labdiff((50, 0, 0), (50, -1, 2)), 2.3669, 4)  # 7
    assert_almost_equal(labdiff((50, -1, 2), (50, 0, 0)), 2.3669, 4)  # 8
    assert_almost_equal(labdiff((50, 2.4900, -0.0010), (50, -2.4900, 0.0009)), 7.1792, 4)  # 9
    assert_almost_equal(labdiff((50, 2.4900, -0.0010), (50, -2.4900, 0.0010)), 7.1792, 4)  # 10
    assert_almost_equal(labdiff((50, 2.4900, -0.0010), (50, -2.4900, 0.0011)), 7.2195, 4)  # 11
    assert_almost_equal(labdiff((50, 2.4900, -0.0010), (50, -2.4900, 0.0012)), 7.2195, 4)  # 12
    assert_almost_equal(labdiff((50, -0.0010, 2.4900), (50, 0.0009, -2.4900)), 4.8045, 4)  # 13
    assert_almost_equal(labdiff((50, -0.0010, 2.4900), (50, 0.0010, -2.4900)), 4.8045, 4)  # 14
    assert_almost_equal(labdiff((50, -0.0010, 2.4900), (50, 0.0011, -2.4900)), 4.7461, 4)  # 15
    assert_almost_equal(labdiff((50, 2.5, 0), (50, 0, -2.5)), 4.3065, 4)  # 16
    assert_almost_equal(labdiff((50, 2.5, 0), (73, 25, -18)), 27.1492, 4)  # 17
    assert_almost_equal(labdiff((50, 2.5, 0), (61, -5, 29)), 22.8977, 4)  # 18
    assert_almost_equal(labdiff((50, 2.5, 0), (56, -27, -3)), 31.9030, 4)  # 19
    assert_almost_equal(labdiff((50, 2.5, 0), (58, 24, 15)), 19.4535, 4)  # 20
    assert_almost_equal(labdiff((50, 2.5, 0), (50, 3.1736, 0.5854)), 1.0000, 4)  # 21
    assert_almost_equal(labdiff((50, 2.5, 0), (50, 3.2972, 0)), 1.0000, 4)  # 22
    assert_almost_equal(labdiff((50, 2.5, 0), (50, 1.8634, 0.5757)), 1.0000, 4)  # 23
    assert_almost_equal(labdiff((50, 2.5, 0), (50, 3.2592, 0.3350)), 1.0000, 4)  # 24
    assert_almost_equal(labdiff((60.2574, -34.0099, 36.2677), (60.4626, -34.1751, 39.4387)), 1.2644, 4)  # 25
    assert_almost_equal(labdiff((63.0109, -31.0961, -5.8663), (62.8187, -29.7946, -4.0864)), 1.2630, 4)  # 26
    assert_almost_equal(labdiff((61.2901, 3.7196, -5.3901), (61.4292, 2.2480, -4.9620)), 1.8731, 4)  # 27
    assert_almost_equal(labdiff((35.0831, -44.1164, 3.7933), (35.0232, -40.0716, 1.5901)), 1.8645, 4)  # 28
    assert_almost_equal(labdiff((22.7233, 20.0904, -46.6940), (23.0331, 14.9730, -42.5619)), 2.0373, 4)  # 29
    assert_almost_equal(labdiff((36.4612, 47.8580, 18.3852), (36.2715, 50.5065, 21.2231)), 1.4146, 4)  # 30
    assert_almost_equal(labdiff((90.8027, -2.0831, 1.4410), (91.1528, -1.6435, 0.0447)), 1.4441, 4)  # 31
    assert_almost_equal(labdiff((90.9257, -0.5406, -0.9208), (88.6381, -0.8985, -0.7239)), 1.5381, 4)  # 32
    assert_almost_equal(labdiff((6.7747, -0.2908, -2.4247), (5.8714, -0.0985, -2.2286)), 0.6377, 4)  # 33
    assert_almost_equal(labdiff((2.0776, 0.0795, -1.1350), (0.9033, -0.0636, -0.5514)), 0.9082, 4)  # 34

    print("All tests passed.", file=sys.stderr)

if __name__ == '__main__':
    test()


# vim: et sts=4 sw=4 tw=79
